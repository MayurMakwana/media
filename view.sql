
CREATE OR REPLACE VIEW vw_total_time_on_channel AS 
 SELECT c.channel_sk,
    d.ad_zone,
    sum(a.time_in_minutes) AS total_time,
    count(DISTINCT d.equipment_sk) AS num_of_viewers
   FROM media_fact a
     JOIN date_dim b ON a.start_date_sk = b.date_sk
     JOIN channel_dim c ON a.channel_sk = c.channel_sk
     JOIN equipment_dim d ON a.equipment_sk = d.equipment_sk
  WHERE a.end_date_sk IS NOT NULL
  GROUP BY c.channel_sk, d.ad_zone;

ALTER TABLE vw_total_time_on_channel
  OWNER TO developer;
