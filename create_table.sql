﻿
CREATE TABLE channel_dim(
	channel_sk INTEGER,
	call_sign VARCHAR(200),
	channel_code INTEGER,
	channel_name VARCHAR(255),
	checksum TEXT,
	last_updated TIMESTAMP
);

CREATE TABLE equipment_dim(
	equipment_sk INTEGER,
	equipment_id VARCHAR(255),
	account_id VARCHAR(255),
	head_end INTEGER,
	zipcode INTEGER,
	ad_zone VARCHAR(255),
	dma_code INTEGER,
	sys_code INTEGER,
	checksum TEXT,
	valid_from TIMESTAMP,
	valid_to TIMESTAMP,
	status BOOLEAN
);

CREATE TABLE media_fact(
	channel_sk INTEGER,
	equipment_sk INTEGER,
	start_date_SK INTEGER,
	end_date_SK INTEGER,
	aff_evt_type_sk INTEGER,
	start_timestamp TIMESTAMP,
	end_timestamp TIMESTAMP,
	time_in_minutes NUMERIC,
	utc_offset VARCHAR(255),
	entry_timestamp TIMESTAMP
);
CREATE TABLE aff_evt_type_dim(
aff_evt_type_sk INTEGER,
affiliation VARCHAR(255),
event_type VARCHAR(50)
);

CREATE TABLE  date_dim AS 
			SELECT
				ROW_NUMBER() OVER (ORDER BY day) as date_sk,
				day as date_pk ,
				rtrim(to_char(day, 'Month')) || to_char(day, ' DD, YYYY') as datedesc,
				to_char(day, 'Day') as day,
				rtrim(to_char(day, 'Month')) as month,
				date_part('year', day) as year,
				CASE
				WHEN date_part('isodow', day) IN (6, 7) THEN True
				ELSE False
				END as isweekday
			FROM
				generate_series('2001-01-01'::date, '2050-12-31'::date, '1 day') day;
	
