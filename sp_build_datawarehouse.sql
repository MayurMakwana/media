﻿-- Procedure to insert a new city
CREATE OR REPLACE FUNCTION build_datawarehouse() 
RETURNS void AS $$
DECLARE SK integer;
BEGIN

/*** channel table ***/

--- load channel data SCD Type 1 
--- create & populate channel stage table 

	CREATE TABLE stg_channel AS 
	SELECT 
		x1.*,
		md5(CAST((x1.channel_code,x1.call_sign,x1.channel_name)AS text)) as checksum, 
		CASE 
			WHEN x2.channel_SK IS NULL
			THEN 'I'
			WHEN md5(CAST((x1.channel_code,x1.call_sign,x1.channel_name)AS text)) != x2.checksum
			THEN 'U'
			ELSE 'X' 
		END as optype
	FROM 
		(
			SELECT 
				distinct 
				channel_code,
				call_sign,
				channel_name 
			FROM 
			stgmedia
		)x1 
	left outer join 
		channel_dim x2 
		on 
		cast(x1.channel_code as INTEGER) = x2.channel_code and 
		x1.channel_name = x2.channel_name;

--- find max SK value caveat no other insert ops on table

	SELECT 
		COALESCE(MAX(channel_sk),0) INTO SK 
	FROM 
	channel_dim;

--- populate channel information
--- insert channel_dim
--- unfortunately postgres doesn't support MERGE(postgres 9.3) to do upsert operation have to perform operation manually.
	INSERT INTO channel_dim
	SELECT  
		row_number() OVER ( ORDER BY a1.call_sign )+SK,
		a1.call_sign,  
		CAST(a1.channel_code as integer) channel_code,
		a1.channel_name,
		a1.checksum,
		CURRENT_TIMESTAMP as last_updated
	FROM stg_channel a1 
	where 
		a1.optype = 'I';

--- udate channel_dim
	UPDATE channel_dim cd
		SET 
		call_sign = stg.call_sign, 
		last_updated = CURRENT_TIMESTAMP,
		checksum = stg.checksum
	FROM 
		stg_channel AS stg
	WHERE 
		cd.channel_code = CAST(stg.channel_code as integer) and 
		cd.channel_name = stg.channel_name and 
		stg.optype = 'U';

---- drop table	
	DROP TABLE stg_channel;
/** end **/
/*** channel table **/

/*** equipment table **/
/** begin **/
	CREATE TABLE tmp_equipment_dim AS 
	select 
	y1.equipment_sk, 
	y1.equipment_id,
	y1.status,
	y1.checksum 
	from 
		equipment_dim y1 
	inner join 
		(
			select 
				equipment_id,
				max(valid_from) as valid_from 
			from 
				equipment_dim
			group by 
			equipment_id
		)y2 
	on 
	y1.equipment_id = y2.equipment_id and 
	y1.valid_from = y2.valid_from;

	CREATE TABLE stg_equipment AS 
	SELECT 
		x1.*,
		md5(CAST((x1.account_id,x1.head_end,x1.zipcode,x1.ad_zone,x1.dma_code,x1.sys_code)AS text)) as checksum,
		CASE 
		WHEN x2.equipment_id is null 
		THEN 'I' 
		WHEN x1.account_id is null and x2.status = true
		THEN 'D'
		WHEN x2.checksum <>  md5(CAST((x1.account_id,x1.head_end,x1.zipcode,x1.ad_zone,x1.dma_code,x1.sys_code)AS text)) 
		THEN 'U'
		WHEN x2.status =false and x1.equipment_id is not null
		THEN 'I'
		ELSE 'X' end as optype,
		x2.status,
		x2.equipment_sk
	FROM 
		(
			SELECT distinct 
				equipment_id,
				account_id,
				head_end,
				zipcode,
				ad_zone,
				dma_code,
				sys_code 
			FROM 
				stgmedia
		)x1
	full outer join 
		tmp_equipment_dim x2 
	on 
		x1.equipment_id = x2.equipment_id;



--- max SK value

	SELECT 
		COALESCE(MAX(equipment_sk),0) INTO SK 
	FROM 
		equipment_dim;

--- insert operation
	INSERT INTO equipment_dim
	SELECT  
		row_number() OVER ( ORDER BY a1.equipment_id )+SK,
		a1.equipment_id,  
		a1.account_id,
		CAST(a1.head_end as INTEGER) head_end,
		CAST(a1.zipcode as INTEGER) zipcode,
		a1.ad_zone,
		CAST(a1.dma_code as INTEGER) dma_code,
		CAST(a1.sys_code as INTEGER) sys_code,
		a1.checksum,
		CURRENT_TIMESTAMP as valid_from,
		TO_TIMESTAMP('9999-12-31','YYYY-MM-DD') as valid_to,
		true as status
	FROM 
		stg_equipment a1 
	where 
		a1.optype IN('U','I');




--- update & delete operation 
	UPDATE equipment_dim ed
	SET 
		valid_to = CURRENT_TIMESTAMP,
		status = false
	FROM 
		stg_equipment AS stg
	WHERE 
		ed.equipment_sk = stg.equipment_sk and 
		stg.optype IN ('D','U');
---  use following filter(where) instead to keep track of only updated rows and ignore any rows that didn't came across in datafile/ more information required.
---  	stg.optype IN ('U');


	DROP table stg_equipment;
	DROP table tmp_equipment_dim;

/** end **/


/*** set up date dimension if doesn't exist **/
	BEGIN
		IF NOT EXISTS(select relname from pg_stat_user_tables where relname ='date_dim')
		THEN
			CREATE TABLE  date_dim AS 
			SELECT
				ROW_NUMBER() OVER (ORDER BY day) as date_sk,
				day as date_pk ,
				rtrim(to_char(day, 'Month')) || to_char(day, ' DD, YYYY') as datedesc,
				to_char(day, 'Day') as day,
				rtrim(to_char(day, 'Month')) as month,
				date_part('year', day) as year,
				CASE
				WHEN date_part('isodow', day) IN (6, 7) THEN True
				ELSE False
				END as isweekday
			FROM
				generate_series('2001-01-01'::date, '2050-12-31'::date, '1 day') day;
		END if;
	END;

/*** date dimension complete **/

/** junk dimension **/

	CREATE TABLE stg_aff_evt_type AS 
	SELECT DISTINCT 
		a.affiliation,
		a.event_type 
	from 
		stgmedia a 
		left outer join 
		aff_evt_type_dim b on 
		a.affiliation = b.affiliation and 
		a.event_type = b.event_type
	WHERE b.aff_evt_type_sk is null
;

--- max SK value

	SELECT COALESCE(MAX(aff_evt_type_sk),0) INTO SK 
	FROM aff_evt_type_dim;
	
	INSERT INTO aff_evt_type_dim
	SELECT 
		ROW_NUMBER() OVER (ORDER BY affiliation)+SK,
		stg.affiliation,
		stg.event_type
	from 
		stg_aff_evt_type stg;

	DROP table stg_aff_evt_type;
	

	
/*** populate fact table **/
	INSERT INTO media_fact
	SELECT 
		b.channel_sk,
		c.equipment_sk,
		d.date_sk as start_date_sk,
		f.date_sk as end_date_sk,
		e.aff_evt_type_sk,
		TO_TIMESTAMP(a.start_timestamp,'YYYY/MM/DD HH24:MI:SS') as start_timestamp,
		TO_TIMESTAMP(a.end_timestamp,'YYYY/MM/DD HH24:MI:SS') as end_timestamp,
		CASE WHEN a.end_timestamp = '' 
		THEN 0
		ELSE 
		round(CAST(EXTRACT(EPOCH FROM (TO_TIMESTAMP(a.end_timestamp,'YYYY/MM/DD HH24:MI:SS') - TO_TIMESTAMP(a.start_timestamp,'YYYY/MM/DD HH24:MI:SS')))/60 AS NUMERIC),0)
		END time_in_minutes,
		a.utc_offset,
		CURRENT_TIMESTAMP as entry_timestamp
	FROM 
		stgmedia a 
	left outer join 
		channel_dim b 
	on 
		CAST( a.channel_code as INTEGER) = b.channel_code and 
		a.channel_name = b.channel_name
	left outer join
		equipment_dim c 
	on 
		a.equipment_id = c.equipment_id and 
		c.status = true
	left outer join 
		date_dim d 
	on 
		TO_DATE(a.start_timestamp,'YYYY/MM/DD') = d.date_pk
	left outer join 
		date_dim f 
	on 
		TO_DATE(a.end_timestamp,'YYYY/MM/DD') = f.date_pk
	left outer join 
		aff_evt_type_dim e
	on
		a.affiliation = e.affiliation and 
		a.event_type = e.event_type;

/** aggregated stats **/

	BEGIN
		IF NOT EXISTS(select relname from pg_stat_user_tables where relname ='agg_channel_15_min_interval_stats')
		THEN
			CREATE TABLE agg_channel_15_min_interval_stats AS 
			SELECT 
			a1 as timestamp_interval,channel_sk,
			count(distinct equipment_sk) as viewers 
			FROM 
			generate_series('2016-05-01 00:15'::timestamp,'2016-08-01 12:00',interval '15 mins')a1
			inner join 
			media_fact b1 
			on 
			a1 between b1.start_timestamp and b1.end_timestamp
			GROUP BY
			a1,channel_sk;
		ELSE 
			TRUNCATE TABLE agg_channel_15_min_interval_stats;
			INSERT INTO agg_channel_15_min_interval_stats
			SELECT 
			a1 as timestamp_interval,channel_sk,
			count(distinct equipment_sk) as viewers 
			FROM 
			generate_series('2016-05-01 00:15'::timestamp,'2016-08-01 12:00',interval '15 mins')a1
			inner join 
			media_fact b1 
			on 
			a1 between b1.start_timestamp and b1.end_timestamp
			GROUP BY
			a1,channel_sk;
			
		END if;
	END;


	

		

END;
$$ LANGUAGE plpgsql;